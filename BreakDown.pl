#!/opt/local/bin/perl

use strict;
use warnings;
use Getopt::Std;
use lib "./inc";
use POSIX;

use Cwd 'abs_path';
use File::Basename;

use File::Temp;
use FindBin qw($Bin);
use lib "$FindBin::Bin";

use Statistics::Descriptive;

my $version='1.1.1';

my %opts = (q=>0,N=>20,R=>10,p=>0.9,m=>5,W=>1000000,G=>1000000,r=>1e-4,e=>1,x=>'ndv',
    F=>'/home/xfan3/reference/human_g1k_v37.fasta',
    U=>1000,
    b=>100,
    g=>0,
    S=>1000000,
    E=>1,
    X=>1000,
    A=>0.8,
    H=>0.7,
    I=>12,
    J=>30,
    P=>30,
    T=>0,
    K=>5,
    Y=>0.5,
    O=>1,
    V=>1000000,
    Z=>0.8,
);

getopts('R:d:zw:q:N:t:s:n:Q:c:p:S:r:x:R:DvF:U:gb:CuBW:X:A:H:e:I:J:P:T:K:Y:O:V:y:G:v',\%opts);
die("
    Usage:   BreakDown.pl <BreakDancer configuration file> <SV file> <Temp GC files>
    Options:
    -v	       Verbose mode
    -d DIR     Directory where all intermediate files are saved
    -c STR     Select chromosome STR
    -g         Turn on GC correction, default: off
    -z         Customized SV format, interpret column from header that must start with # and contain chr1, start, chr2, end, type, size
    -w INT     Specify flanking window size, default: uppercutoff in the configuration file
    -q INT     Only analyze reads with mapping quality >= [$opts{q}]
    -m INT     Minimal number of mismatches in the aligned portion to be tagged as poorly mapped [$opts{m}]
    -N INT     Minimal number of soft-clipped bases to be tagged as breakpoint reads [$opts{N}]
    -p FLOAT   Classify a reads as a breakpoint read if less than [$opts{p}] of bases in a read are unmapped
    -r FLOAT   Prior of SVs in the genome [$opts{r}]
    -x STR     Type of evidences included in genotyping: {n,d,v,nd,nv,dv,ndv} (n: normal, d: discordant, v: variant)
    -F STR     Path to the samtools faidx-ed reference sequence (useful for GC estimation)
    -S 	   Skip sites with size bigger than [$opts{S}]
    -s 	   Skip sites with size smaller than [$opts{s}]
    -a         Estimate allele frequency from all inserts in the outer window, may be useful when boundary is inaccurate, default: normal insert (depth) only in the inner window, trust boundary
    -B 	    For those mapped to different chromosome pair, and one read is soft clipped, count it as a variant if B is on. By default off, so that the inserttype is unknown.
    -E	Count reads as variant inserttype only if the softclipped point is within a certain range [$opts{X}] of the estimated start or end breakpoint. By default on.
    -X	Combined with opts{E}. It is the threshold to count a read as variant if the softclip point is within this range of the estimated start or end breakpoint. By default [$opts{X}]
    -A	The parameter to tune expected discordant reads, should be between 0 and 1. [$opts{A}]
    -H	The parameter to tune expected variant reads, should be between 0 and 1. [$opts{H}]
    -e	The parameter to offset lambda = 0 poisson distribution, so that the new lambda is with $opts{e} added. [$opts{e}]
    -b	Bin size. Should be larger when genome coverage is low. [$opts{b}]
    -I	Expected inserts in each bin. Will decide bin size if greater than that of defined in -b. [$opts{I}]
    -J	The quality threshold below which the reads will be counted as not acceptable. Should be combined with the option of -L. [$opts{J}]
    -P	The mean mapping quality of a bin below which the bin will be not participated in the genotyping decision and AF estimation. [$opts{P}]
    -T	The distance to the two breakpoints, start and end, such that those within the area between two distances are safe bins, which could be counted as neglectable if the fraction of lowqual reads are high. This should also be related with the accuracy of breakpoints. This option is to avoid neglecting bins in flanking region or joint bins. Note: it might be unnecessary.  [$opts{T}]
    -K	The minimal number of normal reads inside a bin below which those outliers will also be taken into account. [$opts{K}] 
    -Y	The parameter ahead of standard deviation to ignore outlier bins, i.e., to reduce bin number. [$opts{Y}]
    -O	The threshold of coefficient of variance. If CV is greater than [$opts{O}] for a bin, the bin will not take part in genotyping and AF estimation. [$opts{O}]
    -V	The threshold of size below which the bin-reduction will not be applied. [$opts{V}]
    -Z	The threshold of percentage of low quality reads so that in initialiation, the bin is ignored to generate the gc-coverage relation. [$opts{Z}]
    -y	The libraries to be ignored, with colon to separate. [$opts{y}]
    -R  Number of regions in initialization step: calculation of insert/bp. [$opts{R}]
    \n") unless ($#ARGV>=1);


my ($f_config, $f_SV, $f_gc)=@ARGV;

$f_gc = "NA" if(!defined $ARGV[2]);
my $bin_size;

if(defined $opts{C}){$opts{g}=1; delete $opts{u};}

# Read SV file
my @SVs=&ReadSVs($f_SV);

# Read cfg file
my $cfg=&ReadBDconfig($f_config, $f_gc);

my @bams;

printf "# %d SVs to be examined.\n", $#SVs+1;
print join("\n", "# from Bams:", @bams) . "\n";

printf "# Version-%s\tParameters: ", $version;
foreach my $opt(keys %opts){printf "\t%s",join(':',$opt,$opts{$opt});} print "\n";

srand(time ^ $$);
my $datadir;
if(defined $opts{d}){
    $datadir=$opts{d};
    mkdir $datadir;
}
else{
    $datadir=File::Temp::tempdir("SV_Assembly_XXXXXX", DIR => '/tmp', CLEANUP => 1);
}

my %GTVocab=('00'=>'homo-ref',
    '01'=>'het-variant',
    '11'=>'homo-variant'
);

# Priors
my %VarPrior=('00'=>log(1-$opts{r}*3/2),
    '01'=>log($opts{r}),
    '11'=>log($opts{r}/2)
);

my %GTPrior=('00'=>log(0.25),
    '01'=>log(0.50),
    '11'=>log(0.25)
);
my $LZERO=-10000000000;
my $LSMALL=$LZERO/2;
my $SMALL=exp($LSMALL);
my $minLogExp = -log(-$LZERO);
my $log10=log(10);
my $PI=3.1415924536;

# Ignored library
my $lib_to_ignore;
if(defined $opts{y} && $opts{y} ne ""){
    if($opts{y}=~ /,/){
        my @libraries_to_ignore = split(/,/, $opts{y});
        foreach my $lib (@libraries_to_ignore){
            $lib_to_ignore->{$lib} = 1;
        }
    }
    else{
        my $lib = $opts{y};
        $lib_to_ignore->{$lib} = 1;
    }
}

# Write header
foreach my $lib(keys %{$$cfg{insert_density}}){
    printf "\# Lib %s: %d inserts per %d bp, on average %.5f insert per bp\n",$lib,$$cfg{insert_density}{$lib}, $opts{G}, $$cfg{insert_density}{$lib}/$opts{G};
    printf "\# With every bin's expected read pair as %d, the bin size would be %d bp. Final bin size is %d bp\n", $opts{I},  $opts{I}*$opts{G}/$$cfg{insert_density}{$lib}, $bin_size->{$lib};
    if(defined $lib_to_ignore->{$lib}){
        printf "\# Lib %s is ignored\n", $lib;
    }
}
print "\#Chr1\tPos1\tChr2\tPos2\tType\tSize\tVarScore\tGenotype\tVarFreq\tGTScore";
print "\tcount1\tcount2\tcount3\tcount4";
print "\tCount\tLL_homoref\tLL_hetvar\tLL_homovar" if(defined $opts{v});
print "\n";

# Run on each SV
foreach my $SV(@SVs){
    if($opts{C}){
        undef $opts{g};
        $opts{x}='n';
        print "#Depth only, no GC correction:\n";
        &GenotypingSV($SV);
        $opts{x}='ndv';
        print "#All inserts, no GC correction:\n";
        &GenotypingSV($SV);

        $opts{g}=1;
        $opts{x}='n';
        print "#Depth only, with GC correction:\n";
        &GenotypingSV($SV);
        $opts{x}='ndv';
        print "#All inserts, with GC correction:\n";
        &GenotypingSV($SV);

        print "\n";
    }
    else{
        &GenotypingSV($SV);
    }
}
print STDERR "AllDone\n";


# The main function for genotyping and VAF estimation
sub GenotypingSV{
    my ($SV)=@_;
    my ($chr1,$start,$chr2,$end,$type,$size)=($SV->{chr1},$SV->{pos1},$SV->{chr2},$SV->{pos2},$SV->{type},$SV->{size});
    # Deal with Deletion and Intra-chromosomal Translocation
    # return if($type !~ /DEL/ && $type !~ /ITX/ && $type !~ /INV/);

    # Count three types of reads for this SV
    my ($InsertCount, $Insert_bar_counts, $Insert_bar_counts_mean_mapping_)=&CountInserts($SV,$opts{w});
    # Estimate the count, and calculate genotype scores and estimate VAF
    my ($AF,$expCounts,$InsertDensity, $gtLogL1,$expCounts1, $hash_count)=&GenotypeScore($SV,$InsertCount,0.5,$opts{x},$Insert_bar_counts,$Insert_bar_counts_mean_mapping_);
my $report_NA;
    if(defined $report_NA && $report_NA eq "NA"){
        printf "%s\t%d\t%s\t%d\t%s\t%d", $chr1,$start,$chr2,$end,$type,$size;
        print "\tNA\tNA\tNA\tNA\n";
        return;
    }

    # Infer the most likely genotype, and the Varscore 
    my $gt_MAP1;
    my $MAP_gt1=$LZERO;
    my $LSUM1=$LZERO;
    my $LSUMgt1=$LZERO;
    my $LNonVar1=$LZERO;
    my %lls1;
    foreach my $gt1(keys %{$gtLogL1}){
        $lls1{$gt1}=$$gtLogL1{$gt1}+$GTPrior{$gt1};
        $LSUMgt1=&LAdd($LSUMgt1,$lls1{$gt1});
        if($MAP_gt1<$lls1{$gt1}){
            $MAP_gt1=$lls1{$gt1};
            $gt_MAP1=$gt1;
        }
        my $ll1=$$gtLogL1{$gt1}+$VarPrior{$gt1};
        $LSUM1=&LAdd($LSUM1,$ll1);
        $LNonVar1=&LAdd($LNonVar1,$ll1) if($gt1 !~ /1/);
    }

    my $LNonMAP1=$LZERO;
    foreach my $gt1(keys %{$gtLogL1}){
        next if($gt1 eq $gt_MAP1);
        $LNonMAP1=&LAdd($LNonMAP1,$lls1{$gt1});
    }
    if(!defined $gt_MAP1){
        printf STDERR "Genotyping failed: %s\t%d\t%s\t%d\t%s\t%d\n", $chr1,$start,$chr2,$end,$type,$size;
        printf "%s\t%d\t%s\t%d\t%s\t%d", $chr1,$start,$chr2,$end,$type,$size;
        printf "\t0\thomo-ref\t0\t0\n";
        return;
    }

    my $varscore1=-10*($LNonVar1-$LSUM1);
    my $gtscore1=-10*($LNonMAP1-$LSUMgt1);

    # Print out result for this SV
    printf "%s\t%d\t%s\t%d\t%s\t%d", $chr1,$start,$chr2,$end,$type,$size;
    printf "\t%d\t%s\t%.3f\t%d",$varscore1,$GTVocab{$gt_MAP1},$AF,$gtscore1 if($AF ne "NA");
    printf "\t%d\t%s\tNA\t%d",$varscore1,$GTVocab{$gt_MAP1},$AF,$gtscore1 if($AF eq "NA");

# print count
#foreach my $key (sort keys %$hash_count){
#print "\t" . $key . ":" . $hash_count->{$key};
#}



    if($opts{v}){
        foreach my $win(keys %{$InsertCount}){
            my $gc_frac = -1;
            foreach my $lib(keys %{$$InsertCount{$win}}){
                printf "\t%s,gc%.3f,%s,c:%.4f,o:%d,x:%d,u:%d,%d,%d,%d",$win,$gc_frac,$lib,$$InsertDensity{$lib},$$InsertCount{$win}{$lib}{outbound}||0,$$InsertCount{$win}{$lib}{unknown}||0,$$InsertCount{$win}{$lib}{unmapped}||0,$$InsertCount{$win}{$lib}{normal}||0,$$InsertCount{$win}{$lib}{discord}||0,$$InsertCount{$win}{$lib}{variant}||0;
                foreach my $gt(sort keys %{$gtLogL1}){
                    printf ",%d,%d,%d",$$expCounts{$gt}{$lib}{n}||0,$$expCounts{$gt}{$lib}{d}||0,$$expCounts{$gt}{$lib}{v}||0;
                }
            }
        }
    }
    print "\n";
}

# The function operated on actual counts and expected counts for VAF and genotype estimation
sub GenotypeScore{
    my ($SV,$InsertCount,$f,$af_est_method,$Insert_bar_counts,$Insert_bar_counts_mean_mapping_)=@_;

my $hash_count;
    # Construction of genotypes
    my @gts=('00','01','11');
    my @fs=(1-$f,$f);
    my %gtlogL;
    my %gtlogL1;
    my $gtlogL1_lib = 0;
    my %expCounts;
    my %expCounts1;
    my %InsertDensity;
    my $vaf = 0;

    my $diff;
    my $normal_bin;
    my $median;

    # Coefficient of variation: standard deviation/mean
    my $CV;
    my $sd;

    # Library to be ignored
    my $ignored_lib;
    foreach my $win(keys %{$InsertCount}){
        my ($chr, $start, $end, $chr1, $chr2);
        if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/ || $SV->{type} =~ /INS/){
            ($chr,$start,$end)=($win=~/^(\S+)\:(\d+)\-(\d+)$/);
        }
        elsif($SV->{type} =~ /ITX/ || $SV->{type} =~ /INV/){
            ($chr, $start, $end) = ($win =~ /^(\S+)\:(\d+)\-\S+\:(\d+)$/);
        }
        else{
            # CTX
            ($chr1, $start, $chr2, $end) = ($win =~ /^(\S+)\:(\d+)\-(\S+)\:(\d+)$/);
        }
        my $w0=$end-$start+1;
        my $b_=($SV->{type} =~ /INV/)?2:1;
        my $str;
        if(defined $opts{g} && $opts{g} == 1 && ($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/)){
            $str = &get_reference_str($win);
        }

        my $c_;

        my $n_normal_in_site = 0; 

        my $i_min;
        foreach my $lib (keys %{$$InsertCount{$win}}){
            $i_min->{$lib} = $$cfg{lowercutoff}{$lib};
        }

        # Denote which bins contribute to AF, per library, sum_lib(wb*sum_i(ci))
        my $n_normal_in_site_expect = 0;

        # Expected discordant and soft-clipped inserts
        my $ad = 0;
        my $as = 0;

        # Pad the SV
        my $start_event = $start + $opts{w};
        my $end_event = $end - $opts{w};
        my ($nn, $nd, $nv);

        # The actual count
        my $nd_ = 0;
        my $nv_ = 0;
        foreach my $lib(keys %{$$InsertCount{$win}}){
            next if(defined $lib_to_ignore->{$lib});
            my $total_ignored = 0;
            my $total_index = 0;
            $normal_bin->{$lib} = Statistics::Descriptive::Full->new();

            # Index is a hash table indicating which indice will be considered to be neglected due to high fraction of low quality reads.
            my $Index;
            my $ignored;
            my $c;
            my $gc_frac_;

            # With GC correction, combine GC/noGC together, both with bins
            my $gc_frac=50;
            if(defined $opts{g} && $opts{g} == 1 && ($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/) ){
                $gc_frac=int(&GCContent($win)*$bin_size->{$lib});
#if($simulation == 1){
#$gc_frac = int(0.5 * $bin_size->{$lib});
#}
            }

            if(defined $$cfg{gc_hist}{$lib}{$gc_frac}){
                $c=$$cfg{gc_hist}{$lib}{$gc_frac}/$opts{U};
            }
            else{
                $c = $$cfg{insert_density}{$lib}/$opts{G};
            }

            # For computing coefficient of variation
            my $start_ = $start;
            my $index = 0;
            my $end_ = $start_;
            # Applied only to unbalanced SVs
            if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
                while($start_ < $end){
                    $end_ = $start_ + $bin_size->{$lib} - 1;
                    $end_ = $end_ > $end ? $end : $end_;
                    my $GCContent_ = 50;
                    if(defined $opts{g} && $opts{g} == 1){
                        my $substr = &substr_($str, $start_, $end_, $start);
                        $GCContent_ = &GCContent_noQuery($substr);
#if($simulation == 1){
#$GCContent_ = 0.5;
#}
                    }
                    my $gc_frac = int($GCContent_*$bin_size->{$lib});
                    $gc_frac_->{$index} = $gc_frac;
                    if(defined $$cfg{gc_hist}{$lib}{$gc_frac}){
                        $c_->{$lib}->{$index ++} = $$cfg{gc_hist}{$lib}{$gc_frac}/$opts{U};
                    }
                    else{
                        $c_->{$lib}->{$index ++} =$$cfg{insert_density}{$lib}/$opts{G};
                    }	

                    my $start_cutoff = $start_event - $b_*$i_min->{$lib};
                    if($start_ > $start_cutoff && $end_ <= $end_event){
                        $Index->{$index-1} = 1 if($start_ > $start_cutoff + $opts{T} && $end_ <= $end_event - $opts{T});
                        if(defined $Index->{$index-1} && defined $Insert_bar_counts_mean_mapping_->{$win}->{$lib}->{$index-1} && $Insert_bar_counts_mean_mapping_->{$win}->{$lib}->{$index-1} < $opts{P}){
                            $ignored->{$index-1} = 1;
                            my $index_ = $index-1;
                        }
                        else{
                            $normal_bin->{$lib}->add_data($Insert_bar_counts->{$win}->{$lib}->{$index-1});

                        }
                    } 
                    $start_ = $end_ + 1;
                }
                $total_ignored += scalar(keys %$ignored);
                $total_index += scalar(keys %$Index);
                if($opts{v}){
                    print "In all there are " . $total_index . " bins, in which " . $total_ignored . " are low quality bins. \n";
                }
                $median->{$lib} = $normal_bin->{$lib}->median();
                $sd->{$lib} = $normal_bin->{$lib}->standard_deviation();
                $CV->{$lib} = $sd->{$lib} / $normal_bin->{$lib}->mean() if($normal_bin->{$lib}->mean() != 0 && defined $sd->{$lib});
                $CV->{$lib} = 0.001 if($normal_bin->{$lib}->mean() < 0.5 * $bin_size->{$lib} * $c || !defined $sd->{$lib});
                if($opts{v}){
                    print "Coefficient variation: " . $CV->{$lib} . "\n";
                    print "CV is greater than the expected value.\n" if($CV->{$lib} > $opts{O});
                }
            } # End of if for unbalanced SVs 

            $InsertDensity{$lib}=$c;
            my $i=$$cfg{mean_insertsize}{$lib};
            my $r=$$cfg{readlen}{$lib};
            $ad += $c*($i-2*$r)*$opts{A};
            $as += 2*$r*$c*$opts{H};
            ($nn,$nd,$nv)=($$InsertCount{$win}{$lib}{normal}||0,
                $$InsertCount{$win}{$lib}{discord}||0,
                $$InsertCount{$win}{$lib}{variant}||0
            );
            $nd_ += $nd;
            $nv_ += $nv;

            # Counting normal estimation
            my $sum;
            my $nc_;
            my $boundary;
            # Applied only to unbalanced SVs
            if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
                foreach my $gt(@gts){
                    my @vs=split //, $gt;
                    my $lambda_n_;
                    $sum->{$gt} = 0;
                    for(my $j = 0; $j <= $#fs; $j ++){
                        my $end_ = $start;
                        my $start_ = $start;
                        my $index = 0;
                        while($end_ < $end){
                            $end_ = $start_ + $bin_size->{$lib};
                            $end_ = $end if($end_ > $end);
                            $boundary->{$index}->{start} = $start_;
                            $boundary->{$index}->{end} = $end_;
                            my $w_ = $end_ - $start_;
                            if($end_ < $start_event - $b_*$i_min->{$lib} || $start_ > $end_event){
                                $nc_->{$gt}->{$index} = 0 if(!defined $nc_->{$gt}->{$index});
                                $nc_->{$gt}->{$index} += $c_->{$lib}->{$index}*$w_*$fs[$j];
                            }
                            elsif($end_ >= $start_event - $b_*$i_min->{$lib} && $start_ <= $end_event){
                                $nc_->{$gt}->{$index} = 0 if(!defined $nc_->{$gt}->{$index});
                                $nc_->{$gt}->{$index} += $c_->{$lib}->{$index}*$w_*$fs[$j]*(1-$vs[$j]);

                            }
                            $start_ = $end_;
                            $index ++;
                        }
                    }

                }

                if($opts{v}){
                    print "Library $lib" . ": \n";
                    foreach my $index (sort {$a<=>$b} keys %{$nc_->{$gts[0]}}){
                        if(defined $ignored->{$index}){
                            next;
                        }
                        my $nc_0 = $nc_->{$gts[0]}->{$index};
                        $nc_0 += $opts{e} if($nc_0 == 0);
                        my $nc_1 = $nc_->{$gts[1]}->{$index};
                        $nc_1 += $opts{e} if($nc_1 == 0);
                        my $nc_2 = $nc_->{$gts[2]}->{$index};
                        $nc_2 += $opts{e} if($nc_2 == 0);
                        print "\n" . join("\t", ($index, $Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_0, $nc_1, $nc_2, &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_0), &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_1), &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_2), $gc_frac_->{$index}, $boundary->{$index}->{start}, $boundary->{$index}->{end}));
                        if(defined $Insert_bar_counts_mean_mapping_->{$win}->{$lib}->{$index}){
                            print "\t" . $Insert_bar_counts_mean_mapping_->{$win}->{$lib}->{$index}; 
                        }
                        else{
                        }
                    }
                }
            }
            my $tag_ = 1;
            # lambda_n is deprecated, expCount{$gt}{$lib}{n} is not used for genotyping
            foreach my $gt(@gts){
                my @vs=split //, $gt;
                my ($lambda_n,$lambda_d,$lambda_v)=(0,0.0,0.0);
                for(my $j=0;$j<=$#fs;$j++){

                    # normal reads expectation
                    if($SV->{type} =~ /DEL/ || $SV->{type} =~ /INS/){
                        my $w=$w0-$vs[$j]*$SV->{size};
                        my $nc=$c*$fs[$j]*($w-$b_*2*$i*$vs[$j]);
                        $nc=($nc>0)?$nc:0;
                        $lambda_n+=$nc;
                    }

                    # discordant read pair expectation
                    my $nd=$c*$fs[$j]*$b_*($i-2*$r)*$vs[$j];
                    if($SV->{type} =~ /DEL/ || $SV->{type} =~ /INS/){
                        $nd=$nd* &LengthDiscordancy($lib,$SV->{size}+$i,$SV);  #adjust for insert size ambiguity
                    }

                    $nd=($nd>0)?$nd:0;
                    $lambda_d+=$nd*$opts{A};

                    # split reads expectation
                    my $nv=$c*$fs[$j]*$b_*2*($r-2*$opts{N})*$vs[$j];
                    $nv=($nv>0)?$nv:0;
                    $lambda_v+=$nv*$opts{H};  

                }
                # Normal reads' contribution to VAF and genotype estimation
                if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
                    if($opts{x}=~/n/ && ($CV->{$lib} <= $opts{O} && $SV->{size} > $opts{V} || $SV->{size} < $opts{V})){
                        $expCounts{$gt}{$lib}{n}+=$lambda_n;
                        # Ignore low quality bin process
                        my @not_ignored_index;
                        my $n_normal_in_site_outlier_expect = 0;
                        my $n_normal_in_site_outlier = 0; 
                        my @ignored_index;
                        foreach my $index (sort {$a <=> $b} keys %{$nc_->{$gt}}){
                            if(defined $ignored->{$index} || defined $sd->{$lib} && abs($Insert_bar_counts->{$win}->{$lib}->{$index} - $median->{$lib}) > $opts{Y} * $sd->{$lib} && $SV->{size} > $opts{V}){
                                if($opts{v}){
                                    if(defined $sd->{$lib} && abs($Insert_bar_counts->{$win}->{$lib}->{$index} - $median->{$lib}) > $opts{Y} * $sd->{$lib} && $tag_ == 1){
                                    }
                                }
                                if(defined $sd->{$lib} && abs($Insert_bar_counts->{$win}->{$lib}->{$index} - $median->{$lib}) > $opts{Y} * $sd->{$lib}){
                                    push @ignored_index, $index;
                                }
                                if($tag_ == 1 && defined $Index->{$index}){
                                    $n_normal_in_site_outlier += $Insert_bar_counts->{$win}->{$lib}->{$index};
                                    $n_normal_in_site_outlier_expect += $c_->{$lib}->{$index} * $bin_size->{$lib};
                                }
                                next;
                            }
                            push @not_ignored_index, $index;
                            my $nc_gt_index_ = $nc_->{$gt}->{$index};
                            $nc_gt_index_ += $opts{e} if($nc_gt_index_ == 0);
                            if($opts{v}){
                                $gtlogL1{$gt} = 0 if(!defined $gtlogL1{$gt});
                                print "The index " . $index . " is being counted. " . &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_gt_index_) . " is being added to " . $gt . ", originally is " . $gtlogL1{$gt} . "\n" if(defined $gtlogL1{$gt});
                            }

                            $gtlogL1{$gt} += &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_gt_index_);
                            if($tag_ == 1 && defined $Index->{$index}){
                                $n_normal_in_site += $Insert_bar_counts->{$win}->{$lib}->{$index};
                                $n_normal_in_site_expect += $c_->{$lib}->{$index} * $bin_size->{$lib};
                            }
                        }
                        if(scalar(@ignored_index) + scalar(@not_ignored_index) < $opts{K} && $SV->{size} > $opts{V}){
                            foreach my $index (@ignored_index){
                                my $nc_gt_index_ = $nc_->{$gt}->{$index};
                                $nc_gt_index_ += $opts{e} if($nc_gt_index_ == 0);
                                $gtlogL1{$gt} += &LogPoissonPDF($Insert_bar_counts->{$win}->{$lib}->{$index}, $nc_gt_index_);
                            }
                            if($tag_ == 1){
                                $n_normal_in_site += $n_normal_in_site_outlier;
                                $n_normal_in_site_expect += $n_normal_in_site_outlier_expect;
                            }
                        }
                        if($opts{v} && $tag_ == 1){
                            print "In all, there are " . scalar(@ignored_index) . " outlier bins, " . scalar(@not_ignored_index) . " normal bins\n"; 
                        }
                        $tag_ = 0;
                    }
                }
                # Discordant reads' contribution to VAF and genotype estimation
                if($opts{x}=~/d/){
                    $expCounts{$gt}{$lib}{d}+=$lambda_d;
                    my $lambda_d_ = $lambda_d;
                    $lambda_d_ += $opts{e} if($lambda_d == 0);
                    my $ld=&LogPoissonPDF($nd,$lambda_d_);
                    $expCounts1{$gt}{$lib}{d}+=$lambda_d;
                    print "Discordant: " . $nd . "," . $lambda_d . ", " . $ld . "\n" if($opts{v});
                    $gtlogL1{$gt}+=$ld;
                }
                # Soft-clipped reads' contribution to VAF and genotype estimation
                if($opts{x}=~/v/){
                    $expCounts{$gt}{$lib}{v}+=$lambda_v;
                    my $lambda_v_ = $lambda_v;
                    $lambda_v_ += $opts{e} if($lambda_v == 0);
                    my $lv=&LogPoissonPDF($nv,$lambda_v_);
                    $expCounts1{$gt}{$lib}{v}+=$lambda_v;
                    print "Variant: " . $nv . "," . $lambda_v . ", " . $lv . "\n" if($opts{v});
                    $gtlogL1{$gt}+=$lv;
                }
            } # End of looping through gt
            # Ignore normal reads if coefficient of variation is large in unbalanced SVs
            if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
                if($CV->{$lib} > $opts{O} && $SV->{size} > $opts{V}){
                    $n_normal_in_site_expect = 0;
                    $n_normal_in_site = 0;
                }
            }
        } # End of library for loop

# dealing with DUP, only tandum duplication now for DRP and SR, for RD only one allele with one copy number gain
if($SV->{type} =~ /DUP/){
#print "DUP: " . $n_normal_in_site_expect . "\t" . $n_normal_in_site . "\n";
$n_normal_in_site -= $n_normal_in_site_expect;
$n_normal_in_site = $n_normal_in_site_expect - $n_normal_in_site;
}

        # Calculating VAF
        if($af_est_method!~/n/ || ($SV->{type} !~ /DEL/ && $SV->{type} !~ /DUP/)){
            $n_normal_in_site_expect = 0;
            $n_normal_in_site = 0;
        }
        if($af_est_method !~/d/){
            $ad = 0;
            $nd_ = 0;
        }
        if($af_est_method !~/v/){
            $as = 0;
            $nv_ = 0;
        }

        if($opts{v}){
            print "nd: " . $nd_ . "\tad: " . $ad . "\n" . "nv: " . $nv_ . "\tas: " . $as . "\n";
        }
        if($af_est_method=~/d/ || $af_est_method=~/v/ || ($SV->{type} !~ /DEL/ && $SV->{type} !~ /DUP/) ){
            my $A = $ad + $as - $n_normal_in_site_expect;
            my $B = -($ad + $as - $n_normal_in_site_expect + $nd_ + $nv_ + $n_normal_in_site);
            my $C = $nd_ + $nv_;
            my $delta=$B*$B-4*$A*$C;
            $vaf=($A!=0)?(-$B-sqrt($delta))/(2*$A):0;

        }
        elsif($af_est_method=~/n/){
		my $B = $n_normal_in_site_expect - $n_normal_in_site;
		my $A = -$n_normal_in_site_expect;
		$vaf=-($B+abs($B))/(2*$A) if($A != 0);
		$vaf = "NA" if($A == 0);

	}
	else{}
	$vaf = 1 if($vaf > 1);

# record count
	$hash_count->{ad} = $ad;
	$hash_count->{as} = $as;
	$hash_count->{nd} = $nd_;
	$hash_count->{nv} = $nv_;
    }

    if($opts{v}){
	    print "gtlogL1:\n";
	    foreach my $gt (@gts){
		    print $gtlogL1{$gt} . "\n";
	    }
    }
    return ($vaf,\%expCounts,\%InsertDensity, \%gtlogL1,\%expCounts1, $hash_count);
}

sub L2_norm{
	my ($nc_, $Insert_bar_count) = @_;
	my $return = 0;
	foreach my $index (sort {$a <=> $b} keys %$nc_){
		if(defined $Insert_bar_count->{$index}){
			$return += ($nc_->{$index} - $Insert_bar_count->{$index})**2;
		}
		else{
			$return += ($nc_->{$index})**2;
		}
	}
	$return = sqrt($return);
	return $return;
}

sub AFDepth{
	my ($SV,$InsertCount)=@_;

	my $alleleFreqs=Statistics::Descriptive::Sparse->new();
	foreach my $win(keys %{$InsertCount}){
		my ($chr,$start,$end)=($win=~/(\S+)\:(\d+)\-(\d+)/);
		my $w0=$end-$start+1;

		my ($total_obs,$total_exp)=(0,0);
		foreach my $lib(keys %{$$InsertCount{$win}}){
			my $c;
			if(defined $opts{g}){
				my $gc_frac=int(&GCContent($win)*$bin_size->{$lib});
#if($simulation == 1){
#$gc_frac = int(0.5*$bin_size->{$lib});
#}
				if(defined $$cfg{gc_hist}{$lib}{$gc_frac}){
					$c=$$cfg{gc_hist}{$lib}{$gc_frac}/$opts{U};
				}
				else{
					$c=$$cfg{insert_density}{$lib}/$opts{G};
				}
			}
			else{  #global average
				$c=$$cfg{insert_density}{$lib}/$opts{G};
			}

			$total_obs+=$$InsertCount{$win}{$lib}{normal}||0;
			$total_exp+=$c*$w0;
		}
		my $vaf=($total_exp>0)?($total_exp-$total_obs)/$total_exp:0;
		$alleleFreqs->add_data($vaf);
	}
	return $alleleFreqs->mean();
}

sub LengthDiscordancy{
	my ($lib,$x,$SV)=@_;
	my ($lp1,$lp2);
	my $v=$$cfg{std_insertsize}{$lib}**2;
	$v += 0.0001 if($v == 0);
	my $m=$$cfg{mean_insertsize}{$lib};
	$lp1=-1/2*log(2*$PI*$v)-($x-$m)**2/(2*$v);

	if($SV->{type} eq 'DEL'){
		$m=$$cfg{mean_insertsize}{$lib}+abs($SV->{size});
		$lp2=-1/2*log(2*$PI*$v)-($x-$m)**2/(2*$v);
	}
	elsif($SV->{type} eq 'INS'){
		$m=$$cfg{mean_insertsize}{$lib}-abs($SV->{size});
		$lp2=-1/2*log(2*$PI*$v)-($x-$m)**2/(2*$v);
	}
	else{
	}

	my $Lsum=&LAdd($lp1,$lp2);
	my $post=exp($lp2-$Lsum);
	return $post;
}

sub CountInserts{
	my ($SV,$win)=@_;
	my ($chr1,$start,$chr2,$end,$type,$size)=($SV->{chr1},$SV->{pos1},$SV->{chr2},$SV->{pos2},$SV->{type},$SV->{size});
	my %ninserts;
	my $Insert_bar_counts;
	my $Insert_bar_counts_mean_mapping_;
# Define windows
	my @windows;
# The window name describing SV, which is to be used in genotyping
	my $window;
	if($chr1 ne $chr2 || $SV->{type} =~ /INV/ || $SV->{type} =~ /ITX/){  # inter-chromosomal, ITX, INV
		my ($l1,$l2)=($start-$win,$start+$win);
		my ($r1,$r2)=($end-$win,$end+$win);
# Add boundary protection
		$l1 = 1 if($l1 < 1);
		$l2 = 1 if($l2 < 1);
		$l1 = $$cfg{LN}{$chr1} if($l1 > $$cfg{LN}{$chr1});
		$l2 = $$cfg{LN}{$chr1} if($l2 > $$cfg{LN}{$chr1});
		$r1 = 1 if($r1 < 1);
		$r2 = 1 if($r2 < 1);
		$r1 = $$cfg{LN}{$chr2} if($r1 > $$cfg{LN}{$chr2});
		$r2 = $$cfg{LN}{$chr2} if($r2 > $$cfg{LN}{$chr2});
		push @windows, $chr1 . ':' . $l1 . '-' . $l2;
		push @windows, $chr2 . ':' . $r1 . '-' . $r2;
		$window = $chr1 . ":" . $start . "-" . $chr2 . ":" . $end;
	}
	elsif($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/ || $SV->{type} =~ /INS/){ # DEL, DUP, INS
		my ($l,$r)= ($start-$win, $end+$win);
		if($l<1 || $r>$$cfg{LN}{$chr1}){
			printf STDERR "skip genotyping, window out of bound:%s \n", join('.',$chr1,$start,$chr2,$end,$type);
			return;
		}
		if($r-$l>$opts{W}){
			print STDERR "window size too large skip ... " . join(".",$chr1,$start,$chr2,$end,$type,$size) . "\n";
		}
		else{
			push @windows, $chr1 . ':' . $l . '-' . $r;
		}
		$window = $chr1 . ':' . $start . '-' . $end;
	}

	return unless ($#windows>=0);

	my %Inserts;
	my ($chr, $left, $right);
	foreach my $win(@windows){  #Test each window
		($chr,$left,$right)=($win=~/(\S+)\:(\d+)\-(\d+)/);
# Count the paired end reads in each bin
		foreach my $bam(@{$$cfg{bams}}){
			my $nreads=`samtools view -c -q $opts{q} $bam $win`;
			chomp $nreads;
			my $read_density=$nreads/($right-$left+1);
			if($read_density>1000*$$cfg{median_readDensity}{$bam}){
				print STDERR "Depth too high in $win, skip ...\n";
				next;
			}
			my $scmd="samtools view -q $opts{q} $bam $win";	
			open(SAM,"$scmd |");
			while(<SAM>){
				chomp;
				my $r;
				($r->{name},$r->{flag},$r->{chr},$r->{pos},$r->{MAPQ},$r->{cigar},$r->{mchr},$r->{mpos},$r->{isize},$r->{seq},$r->{qual})=split /\t/;
				next if($r->{flag} & 0x0200 || $r->{flag} & 0x0400);  # Duplicates or failed vendor QC
					($r->{numMismatch})=($_=~/XM\:i\:(\d+)/);
				$r->{numMismatch}=0 if(!defined $r->{numMismatch});
				next if($r->{numMismatch}>$opts{m});

				if($r->{flag} & 0x10 && $r->{mpos}<$r->{pos} && $r->{cigar}=~/(\d+)S$/){
					$r->{softclipped}=$1;
				}
				elsif($r->{flag} & 0x20 && $r->{pos} < $r->{mpos} && $r->{cigar}=~/^(\d+)S/){
					$r->{softclipped}=$1;
				}
				else{
					$r->{softclipped}=0;
				}

				$r->{isize}=abs($r->{isize});
				($r->{MAQFlag})=($_=~/MF\:i\:(\d+)/);  # MAQ flag
					$r->{numBaseMapped}=0;
				foreach my $mstr(split /M/,$r->{cigar}){
					my ($nbasem)=($mstr=~/(\d+)$/);
					$r->{numBaseMapped}+=$nbasem || 0;
				}
				$r->{name}=~s/\/[12]//;
				$r->{sam}=$_;

				if(/RG\:Z\:(\S+)/){
					my $readgroup=$1;
					$r->{library}=$$cfg{readgroup_library}{$readgroup} if(defined $$cfg{readgroup_library}{$readgroup});
				}
				else{
					$r->{library}=basename($bam);
				}
				push @{$Inserts{$r->{library}}{$r->{name}}}, $r if(defined $r->{library});
			}
			close(SAM);
		}
	}

# Rewrite the bar_count according to each library
	my $bar_count;
	foreach my $lib(keys %Inserts){
		my $Insert_bar_count;		
		my $Insert_bar_count_lowqual;
		my $Insert_bar_count_mean_mapping_;
		my $Insert_bar_count_mean_mapping_weight;
		if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
			next if(defined $lib_to_ignore->{$lib});
			$bar_count->{$lib} = ceil(($right - $left)/$bin_size->{$lib});
			foreach my $i (0 .. $bar_count->{$lib}-1){
				$Insert_bar_count->{$i} = 0;
				$Insert_bar_count_lowqual->{$i} = 0;
			}
		}
		my @buffer;
		foreach my $insert(keys %{$Inserts{$lib}}){
			my ($r1,$r2)=@{$Inserts{$lib}{$insert}};
			my $inserttype='unknown';
			my $insert_count=1;

			if(defined $r2){  #both ends are in
				my $max_MAPQ=($r1->{MAPQ}>$r2->{MAPQ})?$r1->{MAPQ}:$r2->{MAPQ};
				my $min_MAPQ=($r1->{MAPQ}<$r2->{MAPQ})?$r1->{MAPQ}:$r2->{MAPQ};
				my $min_NM=($r1->{numMismatch}<$r2->{numMismatch})?$r1->{numMismatch}:$r2->{numMismatch};
				if($max_MAPQ>=$opts{q} && $min_NM<$opts{N}){  #at least one of the ends has sufficient mapping quality
					push @buffer,$r1->{name};
					push @buffer,$r1->{seq};
					push @buffer,$r2->{name};
					push @buffer,$r2->{seq};

#                    if($r1->{pos}>=$left && $r1->{pos}<=$right ||  #at least one read mapped into the window
#                       $r2->{pos}>=$left && $r2->{pos}<=$right){
	if($r1->{chr} eq $r2->{chr}){  #same chromosome
		if(
				$r1->{softclipped}>=$opts{N} ||  # ends softclipped
				$r2->{softclipped}>=$opts{N} 
		  ){

			if($opts{E}){
				my $range = $opts{X};
				if(abs($start - $r1->{pos}) < $range || abs($start - $r2->{pos}) < $range || abs($end - $r1->{pos}) < $range || abs($end - $r2->{pos}) < $range){
					$inserttype = 'variant';
				}
			}
			else{ 
				$inserttype='variant';
			}

		}
		elsif($r1->{flag} & 0x0002 # the read is mapped in a proper pair 
		     ){

			if($SV->{type} =~ /DEL/ || $SV->{type} =~ /INS/ || $SV->{type} =~ /DUP/){
if($SV->{type} !~ /DUP/){
				$insert_count=&LengthDiscordancy($lib,$r1->{isize},$SV);
}
				if($SV->{type} !~ /DUP/ && $insert_count>0.5){
					$inserttype='discord';
				}
				else{
					$inserttype='normal';
					$insert_count=1-$insert_count;
					$Insert_bar_count->{&get_bar_pos($r1->{pos}, $r2->{pos}, $left, $bin_size->{$lib})} += 1; 
					my ($index_tmp) = &find_overlapping_index($left, $bin_size->{$lib}, $r1->{pos}, $r1->{flag}, $r1->{cigar}, $lib);
					foreach my $index_ (keys %{$index_tmp}){
						my $weight_ = $index_tmp->{$index_};
						$Insert_bar_count_mean_mapping_->{$index_} += $r1->{MAPQ}*$weight_;
						$Insert_bar_count_mean_mapping_weight->{$index_} += $weight_;
					}
					($index_tmp) = &find_overlapping_index($left, $bin_size->{$lib}, $r2->{pos}, $r2->{flag}, $r2->{cigar}, $lib);
					foreach my $index_ (keys %{$index_tmp}){
						my $weight_ = $index_tmp->{$index_};
#											}
					$Insert_bar_count_mean_mapping_->{$index_} += $weight_*$r2->{MAPQ};
					$Insert_bar_count_mean_mapping_weight->{$index_} += $weight_;
				}
			}
		}
	}
	else{ # not properly mapped
		if($r1->{flag} & 0x0004 ||
				$r2->{flag} & 0x0004
		  ){
			if($SV->{type} =~ /INS/){
				$inserttype='discord';
			}
			else{
				$inserttype='unmapped';  # unmapped reads, potentially variant supporting
			}
		}
		elsif($r1->{flag} & 0x0010 && $r1->{flag} & 0x0020 ||
				(!$r1->{flag} & 0x0010) && (!$r1->{flag} & 0x0020)
		     ){  # inversion
			$inserttype='discord' if($SV->{type} =~ /INV/);
		}
		elsif($r1->{flag} & 0x0010 && ! ($r1->{flag} & 0x0020) && $r1->{pos}<$r1->{mpos} || $r1->{flag} & 0x0020 && ! ($r1->{flag} & 0x0010) && $r1->{pos}>$r1->{mpos}){
			$inserttype='discord' if($SV->{type} eq 'DUP'); # interspersed DUP might be different TODO
		}
		else{
			if(($r1->{pos}<$r1->{mpos} && $r1->{flag} & 0x0020 ||
						$r1->{pos}>$r1->{mpos} && $r1->{flag} & 0x0010)){ # orientation and position normal
				if($SV->{type} =~ /DEL/ || $SV->{type} =~ /INS/){  # length discrepancy
					if($SV->{type} =~ /DEL/){ # DEL
						if($opts{D} && $r1->{isize}>2*$$cfg{mean_insertsize}{$lib}){
							$inserttype='discord';
						}
						else{
							$insert_count=&LengthDiscordancy($lib,$r1->{isize},$SV);
							if($insert_count>0.5){
								$inserttype='discord';
							}
							else{
								$inserttype='normal';
								$insert_count=1-$insert_count;
								$Insert_bar_count->{&get_bar_pos($r1->{pos}, $r2->{pos}, $left, $bin_size->{$lib})} += 1; 
# deal with the left read for mean mapping quality
								my $index_tmp = &find_overlapping_index($left, $bin_size->{$lib}, $r1->{pos}, $r1->{flag}, $r1->{cigar}, $lib);
								foreach my $index_ (keys %{$index_tmp}){
									my $weight_ = $index_tmp->{$index_};
									$Insert_bar_count_mean_mapping_->{$index_} += $r1->{MAPQ}*$weight_;
									$Insert_bar_count_mean_mapping_weight->{$index_} += $weight_;
								}

# deal with the right read for mean mapping quality
								($index_tmp) = &find_overlapping_index($left, $bin_size->{$lib}, $r2->{pos}, $r2->{flag}, $r2->{cigar}, $lib);
								foreach my $index_ (keys %{$index_tmp}){
									my $weight_ = $index_tmp->{$index_};
									$Insert_bar_count_mean_mapping_->{$index_} += $weight_*$r2->{MAPQ};
									$Insert_bar_count_mean_mapping_weight->{$index_} += $weight_;
								}

							}
						}
					}
					elsif($SV->{type} =~ /INS/){ # INS
						if($opts{D} && $r1->{isize}<$$cfg{mean_insertsize}{$lib}-2*$$cfg{std_insertsize}{$lib}){
							$inserttype='discord';
						}
					}
				}
			}
			else{  # everted alignment, corresponding to many possibilities
				$inserttype='discord' if($SV->{type} eq 'ITX');
			}
		}
	}
}
else{  # different chromosome or unmapped
	if(
			$r1->{softclipped}>=$opts{N} ||  # one of the pair softclipped
			$r2->{softclipped}>=$opts{N} 
	  ){
		$inserttype='variant' if($opts{B});  # unknown variant type
	}
	else{   # mapped to different chromosome, CTX
		$inserttype='discord' if($SV->{type} eq 'CTX');
	}
}
}
#                    else{  # none of the reads mapped into the window, ignore
#                        $inserttype='outbound';
#                    }
#            }
else{
# ignore low mapping quality reads
	$inserttype='lowqual';
}
}
else{  # only one read in the window
# ITX, INV, CTX computations
	if($r1->{MAPQ}>=$opts{q}){
#if( $r1->{pos}>=$left && $r1->{pos}<=$right && $r1->{mchr} ne '='){
	if($r1->{mchr} ne '='){
		push @buffer,$r1->{name};
		push @buffer,$r1->{seq};
		if($r1->{flag} & 0x0001 ){  # Paired-end reads
			$inserttype='discord' if($SV->{type} eq 'CTX' && $r1->{mchr} eq $SV->{chr2});
		}
		else{  # Single-end reads
			if($r1->{softclipped}>=$opts{N} 
# Too many mismatches or mismatch at a particular position (for snps)
			  ){
				$inserttype='variant' if($opts{B});  # unknown variant type
			}
			else{
				$inserttype='normal';
				$Insert_bar_count->{&get_bar_pos($r1->{pos}, "NA", $left, $bin_size->{$lib})} += $insert_count/2;
				my ($index_tmp) = &find_overlapping_index($left, $bin_size->{$lib}, $r1->{pos}, $r1->{flag}, $r1->{cigar}, $lib);
				foreach my $index_ (keys %{$index_tmp}){
					my $weight_ = $index_tmp->{$index_};
					$Insert_bar_count_mean_mapping_->{$index_} += $r1->{MAPQ}*$weight_;
					$Insert_bar_count_mean_mapping_weight->{$index_} += $weight_;
				}

			}
		}
	}
	else{
		$inserttype='outbound';
	}
}
else{
	$inserttype='lowqual';
}
}

$inserttype = 'normal' if($inserttype eq "");
$ninserts{$window}{$lib}{$inserttype}+=$insert_count;
if($inserttype eq 'unmapped' && defined $opts{u}){
	$ninserts{$window}{$lib}{variant}+=$insert_count/2;
	$ninserts{$window}{$lib}{discord}+=$insert_count/2;
}
}

if($SV->{type} =~ /DEL/ || $SV->{type} =~ /DUP/){
	foreach my $index (keys %$Insert_bar_count_mean_mapping_){
		$Insert_bar_counts_mean_mapping_->{$window}->{$lib}->{$index} = $Insert_bar_count_mean_mapping_->{$index}/$Insert_bar_count_mean_mapping_weight->{$index};
	}

# Avoid calls in a region with extremely high coverage
	foreach my $index (keys %$Insert_bar_count){
		my $tag = -1;
		foreach my $bam (keys %{$$cfg{median_readDensity}}){
			if($Insert_bar_count->{$index}/$bin_size->{$lib} > 1000*$$cfg{median_readDensity}{$bam}){
				$tag = 1;
				last;
			}
		}
		if($tag == 1){
			print STDERR "Depth too high in $window, skip ...\n";
			return;
		}
	}

	$Insert_bar_counts->{$window}->{$lib} = $Insert_bar_count;
}
my $freads=$datadir . '/' . join('.',$chr1,$start,$chr2,$end,$type,$window,$lib) . '.fa';
if(defined $opts{d} && (!-s $freads)){  #create a new file
	if($#buffer<0){print STDERR "no qualified reads from bam, skip ... \n"; return;}

	open(OUT,">$freads") || die "unable to open $freads\n";
	while(@buffer){
		printf OUT ">%s\n",shift @buffer;
		my $sequence=shift @buffer;
		printf OUT "%s\n",$sequence;
	}
	close(OUT);
}
}
return (\%ninserts, $Insert_bar_counts, $Insert_bar_counts_mean_mapping_); 
}

# Find the overlapping index with the current read, given the position, flag and cigar string
sub find_overlapping_index{
	my ($start, $bin_size_, $read_left_pos, $flag, $cigar, $lib) = @_;
	my $hash;
	my @numbers;
	my @strings;
	my $total = $$cfg{readlen}{$lib};
	my $region_start = $read_left_pos;
	my $region_end = $read_left_pos + $total - 1;
	if(length($cigar) != 0 && $cigar =~ /^(\d+)s/){
		$region_start += $1;
		$cigar =~ s/^\d+\S//;
	}
	if(length($cigar) != 0 && $cigar =~ /(\d+)s$/){
		$region_end -= $1;
	}

	my $bin_pos = &get_bar_pos($region_start, "NA", $start, $bin_size_);
	my $bin_start = $start + $bin_pos * $bin_size_;
	my $bin_end = $bin_start + $bin_size_ - 1;

	my $overlap_length = &overlap_length($region_start, $region_end, $bin_start, $bin_end); 
	$hash->{$bin_pos} = $overlap_length if($overlap_length != 0);
	while($overlap_length != 0){
		$bin_pos ++;
		$region_start = $bin_start + $bin_size_ + 1;
		last if($region_start > $region_end);
		$bin_start += $bin_size_;
		$bin_end += $bin_size_;
		$overlap_length = &overlap_length($region_start, $region_end, $bin_start, $bin_end);
		$hash->{$bin_pos} = $overlap_length + 1 if($overlap_length != 0);
	}
	return $hash;
}


# Return the index of bin with bin length 
sub get_bar_pos_length{
	my ($start_position, $end_position, $start, $bin_size_, $pos) = @_;
	my $bin_start = $start + $bin_size_ * $pos;
	my $bin_end = $start + $bin_size_ * ($pos + 1) - 1;
	my ($length) = &overlap_length($bin_start, $bin_end, $start_position, $end_position);
	return $length;
}

# Given two regions with format "start-end", return the length of the overlapping sequence
sub overlap_length{
	my ($start1, $end1, $start2, $end2) = @_;
	my $length = 0;
	if($start2 < $end1 && $start2 >= $start1){
		$length = ($end2-$start2)>($end1-$start2)?($end1-$start2):($end2-$start2);
	}
	elsif($start1 < $end2 && $start1 >= $start2){
		$length = ($end1-$start1)>($end2-$start1)?($end2-$start1):($end1-$start1);
	}
	return ($length);
}

# Get the index of bin 
sub get_bar_pos{
	my ($pos1, $pos2, $start, $bin_size_) = @_;
	if($pos2 ne "NA"){
		my $start_pos = $pos1 > $pos2 ? $pos2:$pos1;
		my $pos = floor(($start_pos - $start)/$bin_size_);
		return $pos;
	}
	else{
		my $pos = floor(($pos1 - $start)/$bin_size_);
		return $pos;
	}
}

# Get the substring with the particular position
sub substr_{
	my ($str, $start, $end, $zero_position) = @_;
	my $return_str = substr $str, $start - $zero_position, $end - $start + 1;
	return $return_str;
}

# Get the reference sequence from faidx
sub get_reference_str{
	my ($win) = @_;
	my $str;
	open(FA, "samtools faidx $opts{F} $win |");
	while(<FA>){
		next if(/\>/);
		chomp;
		$str .= $_;
	}
	close(FA);
	return $str;
}

# Compute GC content without query to faidx
sub GCContent_noQuery{
	my ($str) = @_;
	my @gc = ($str=~ /[gc]/ig);
	my $len = length($str);
	my $gc_frac = ($len<=0)?0.5:scalar(@gc)/$len;
	return $gc_frac;
}


sub GCContent{
	my ($win)=@_;
	my ($len,$gclen)=(0,0);
	open(FA,"samtools faidx $opts{F} $win |");
	while(<FA>){
		next if(/\>/);
		chomp;
		my @gc=($_=~/[gc]/ig);
		$len+=length($_);
		$gclen+=scalar @gc;
	}
	close(FA);
	my $gc_frac=($len<=0)?0.5:$gclen/$len;
	return $gc_frac;
}

sub ReadSVs{
	my ($f)=@_;
	my @coor;
	open(IN,"<$f") || die "unable to open $f\n";
	if($opts{z}){   # Read custom formated file
		my %hc;
		while(<IN>){
			chomp;
			my @cols=split /\t+/;
			if(/^\#/){
				for(my $i=0;$i<=$#cols;$i++){
					$hc{chr1}=$i if(!defined $hc{chr1} && $cols[$i]=~/chr1/i);
					$hc{pos1}=$i if(!defined $hc{pos1} && $cols[$i]=~/start/i);
					$hc{chr2}=$i if(!defined $hc{chr2} && $cols[$i]=~/chr2/i);
					$hc{pos2}=$i if(!defined $hc{pos2} && $cols[$i]=~/end/i);
					$hc{type}=$i if(!defined $hc{type} && $cols[$i]=~/type/i);
					$hc{size}=$i if(!defined $hc{size} && $cols[$i]=~/size/i);
				}
				$hc{chr2}=$hc{chr1} if(!defined $hc{chr2} && defined $hc{chr1});
				next;
			}
			die "file header in correctly formated.  Must have \#, chr, start, end, type, size.\n" if(!defined $hc{chr1} || !defined $hc{pos1} || !defined $hc{pos2} || !defined $hc{type} || !defined $hc{size});
			my $cr;
			$cr->{line}=$_;
			foreach my $k('chr1','pos1','chr2','pos2','type','size'){
				$cr->{$k}=$cols[$hc{$k}];
			}

			next unless(defined $cr->{pos1} && defined $cr->{pos2} && $cr->{pos1}=~/^\d+$/ && $cr->{pos2}=~/^\d+$/);
			next if(defined $opts{t} && $opts{t} ne $cr->{type} ||
					defined $opts{s} && abs($cr->{size})<$opts{s} ||  #size too small
					defined $opts{S} && abs($cr->{size})>$opts{S} ||  #size too big
					defined $opts{c} && $cr->{chr1} ne $opts{c}
			       );

#$cr->{chr1}=~s/^chr//;
#$cr->{chr2}=~s/^chr//;
			$cr->{size}=abs($cr->{size});
			$cr->{size} = abs($cr->{pos2}-$cr->{pos1}) if($cr->{chr1} eq $cr->{chr2} && $cr->{type} =~ /del/i);
			push @coor, $cr;
		}
	}
	else{  # Read BreakDancer formated file
		my @col_cn;
		my ($col_gene,$col_database);
		my @headfields;
		my @nlibs=split /\,/,$opts{L} if(defined $opts{L});
		while(<IN>){
			chomp;
			if(/^\#/){
				next unless(/Chr1\s/);
# Parse the header line
				@headfields=split /\t/;
				my $col_normalcn;
				for(my $i=0; $i<=$#headfields; $i++){
					if($headfields[$i]=~/\.bam/){
						if($headfields[$i]=~/normal/i){
							$col_normalcn=$i;
						}
						else{
							push @col_cn,$i;
						}
					}
					elsif($headfields[$i]=~/Gene/i){
						$col_gene=$i;
					}
					elsif($headfields[$i]=~/databases/i){
						$col_database=$i;
					}
				}
				if(defined $col_normalcn){
					unshift @col_cn, $col_normalcn;
				}
			}
			chomp;
			my $cr;
			my @fields=split;
			my @extra;
			(
			 $cr->{chr1},
			 $cr->{pos1},
			 $cr->{ori1},
			 $cr->{chr2},
			 $cr->{pos2},
			 $cr->{ori2},
			 $cr->{type},
			 $cr->{size},
			 $cr->{score},
			 $cr->{nreads},
			 $cr->{nreads_lib},
			 @extra
			)=@fields;
			$cr->{line}=$_;

#$cr->{chr1}=~s/^chr//;
#$cr->{chr2}=~s/^chr//;
			if(defined $opts{M}){$cr->{chr1}="chr".$cr->{chr1}; $cr->{chr2}="chr".$cr->{chr2};}

			next if($cr->{chr1}=~/NT/ || $cr->{chr1}=~/RIB/);
			next if($cr->{chr2}=~/NT/ || $cr->{chr2}=~/RIB/);

			next unless(defined $cr->{pos1} && defined $cr->{pos2} && $cr->{pos1}=~/^\d+$/ && $cr->{pos2}=~/^\d+$/ && $cr->{size}=~/\d+$/);
			$cr->{size}=abs($cr->{size});
			$cr->{size} = abs($cr->{pos2}-$cr->{pos1}) if($cr->{chr1} eq $cr->{chr2} && $cr->{type} =~ /del/i);
			next if(defined $opts{t} && $opts{t} ne $cr->{type} ||
					defined $opts{s} && abs($cr->{size})<$opts{s} || # size too small
					defined $opts{S} && abs($cr->{size})>$opts{S} || # size too big
					defined $opts{n} && $cr->{nreads}<$opts{n} ||
					defined $opts{Q} && $cr->{score}<$opts{Q} ||
					defined $opts{c} && $cr->{chr1} ne $opts{c}
			       );

# Ignore events detected in a library
			my $ignore=0;
			foreach my $nlib(@nlibs){
				$ignore=1 if($cr->{nreads_lib}=~/$nlib/);
			}

# Include Copy Number Altered events if available
			if(@col_cn){
				for(my $i=1;$i<=$#col_cn;$i++){
					next if($fields[$col_cn[$i]]=~/NA/ || $fields[$col_cn[0]]=~/NA/);
					my $diff_cn=abs($fields[$col_cn[$i]]-$fields[$col_cn[0]]);
				}
				my @cns;
				for(my $i=0;$i<=$#col_cn;$i++){
					push @cns,join(':',$headfields[$col_cn[$i]],$fields[$col_cn[$i]]);
				}
				$cr->{cnstr}=join(',',@cns);
			}

			$cr->{gene}=$fields[$col_gene] if($col_gene);
			$cr->{database}=$fields[$col_database] if($col_database);
			$ignore=0 if($cr->{line}=~/cancer/i || $cr->{line}=~/coding/i);
			$ignore=0 if($cr->{line}=~/gene/i && $cr->{type}=~/ctx/i);  # assemble all translocation overlapping gene
				next if($ignore>0);
			push @coor, $cr;
		}
	}
	close(IN);
	return @coor;
}

# check if any overlap; simplified version for small num of SV
sub check_overlap{
	my ($chr, $pos, $SV_trunk) = @_;
# only check if chr the same, if off + len is smaller for this to be inserted SV
my ($pos1, $pos2) = ($pos =~ /.+\:(\d+)\-(\d+)/);
	for my $SV (@$SV_trunk){
next if($SV->{type} !~ /DUP/ && $SV->{type} !~ /DEL/ || $SV->{chr1} ne $SV->{chr2} || $SV->{chr1} ne $chr);
		my ($start, $end) = ($SV->{pos1}, $SV->{pos2});
		if($pos1 < $end && $pos1 > $start || $start < $pos2 && $start > $pos1){
			return 0;
		}
	}
	return 1;
}


sub ReadBDconfig{
	my ($f_config, $f_gc)=@_;
	my %cfgs;
	my %fbams;
	my $maxisize=0;

	open(CONFIG,"<$f_config") || die "unable to open $f_config\n";
	while(<CONFIG>){
		next unless (/\S+/);
		next if(/^\#/);
		chomp;
		my $fh;
		my ($fbam)=($_=~/map\:(\S+)\b/i);
		if(!-e $fbam){
			my $dirname = dirname(abs_path($f_config));
			$fbam = $dirname . "/" . $fbam;
		}
		my ($mean)=($_=~/mean\w*\:(\S+)\b/i);
		my ($std)=($_=~/std\w*\:(\S+)\b/i);
		my ($insertperbp) = ($_=~/insertperbp\w*:(\S+)\b/i);
		my ($readlen)=($_=~/readlen\w*\:(\S+)\b/i);
		my ($upper,$lower);
		($upper)=($_=~/upp\w*\:(\S+)\b/i);
		($lower)=($_=~/low\w*\:(\S+)\b/i);
		my ($mqual)=($_=~/map\w*qual\w*\:(\d+)\b/i);
		my ($lib)=($_=~/lib\w*\:(\S+)\b/i);
		($lib)=($_=~/samp\w*\:(\S+)\b/i) if(!defined $lib);
		undef $lib if($lib eq 'NA');
		$lib=basename($fbam) if(!defined $lib);
		$fbams{$fbam}++ if(defined $fbam);

		my ($readgroup)=($_=~/group\:(\S+)\b/i);
		undef $readgroup if($readgroup eq 'NA');
		$readgroup=$lib if(!defined $readgroup);
		$cfgs{readgroup_library}{$readgroup}=$lib;

		my ($platform)=($_=~/platform\:(\S+)\b/i);
		$cfgs{readgroup_platform}{$readgroup}=$platform;  # default to illumina

			my ($exe)=($_=~/exe\w*\:(.+)\b/i);
		$cfgs{library_map}{$lib}=$fbam;
		$cfgs{mapQual_cutoff}{$lib}=$mqual if(defined $mqual && ($mqual=~/^\d+$/));
		$cfgs{map_library}{$fbam}{$lib}++;

		if(defined $mean && defined $std && (!defined $upper || !defined $lower)){
			$upper=$mean+$std*3;
			$lower=$mean-$std*3;
			$lower=($lower>0)?$lower:0;
		}

		$cfgs{mean_insertsize}{$lib}=$mean;
		$cfgs{std_insertsize}{$lib}=$std;
		$cfgs{uppercutoff}{$lib}=$upper;
		$maxisize=($maxisize<$upper)?$upper:$maxisize;
		$cfgs{lowercutoff}{$lib}=$lower;
		$cfgs{readlen}{$lib}=$readlen;
		$cfgs{insert_density}{$lib} = $insertperbp * $opts{G} if(defined $insertperbp);
		$cfgs{median_readDensity}{$fbam} += $insertperbp if(defined $insertperbp);
		$opts{x} =~ s/d// if($cfgs{mean_insertsize}{$lib} - 2*$cfgs{readlen}{$lib} <= 0);
	}
	close(CONFIG);

	my @bams=keys %fbams;

	my %readDensity;
	open(cfg_new, ">cfg.tmp") if($f_gc eq "NA");
	foreach my $bam(@bams){
		$readDensity{$bam}=Statistics::Descriptive::Full->new();
		my @chrs;
		my $scmd="samtools view -H $bam";	
		open(SAM,"$scmd |");
		while(<SAM>){
			if(/^\@SQ/){
				my ($chr,$ln)=($_=~/\@SQ\tSN\:(\S+)\tLN\:(\d+)/);
				$cfgs{LN}{$chr}=$ln;
				push @chrs,$chr if($ln>$opts{G} && ($chr=~/^\w*\d+$/));
			}
		}
		close(SAM);

		my @regs;
# select random regions
		srand(time ^ $$);
		for(my $i=0;$i<$opts{R};$i++){
			my ($idxchr,$random_chr);
			if(defined $opts{c}){
				$random_chr=$opts{c};
			}
			else{
				do {
					$idxchr=int(rand()*($#chrs+1));
					$random_chr=$chrs[$idxchr] if(defined $chrs[$idxchr] && $chrs[$idxchr]=~/\d$/);  #autosome
				} until (defined $random_chr);
			}
# The random selection of regions to compute insert size includes only the reads that have a qualified mapping quality.
			my $no_overlapping = 0;
my $reg;
			while($no_overlapping == 0){
				my $random_start = int(rand() * ($cfgs{LN}{$random_chr}-$opts{G}))+1;
				$reg=$random_chr.':'.$random_start.'-'.($random_start+$opts{G}-1);
				$no_overlapping = &check_overlap($random_chr, $reg, \@SVs);
			}
			push @regs,$reg;
		}

		my %stats;
		my %gc_hist;
		my @libs=keys %{$cfgs{map_library}{$bam}};

# Read the gc file
		if($f_gc ne "NA"){
			my $f_gc_ = `ls $f_gc`;
			my @f_gcs = split("\n", $f_gc_);
			foreach my $file_gc (@f_gcs){
				my ($lib) = ($file_gc =~ /^.+tmp\.(.+)$/);
				if(defined $cfgs{map_library}{$bam}{$lib}){
					$cfgs{gc_file}{$bam}{$lib} = $file_gc;
				}
			}
		}

# If gc file is existing
		if(defined $cfgs{gc_file}{$bam}){
# assume as long as bam is the key, all libraries in the bam is added
			&read_gc_file(\%gc_hist, \%cfgs);
			&read_new_cfg("cfg.tmp", \%cfgs, $f_config);
		}
# If no gc file, but GC correction is on
		elsif(defined $opts{g} && $opts{g}){
			$stats{$libs[0]}=Statistics::Descriptive::Full->new();
			$stats{$libs[0]}->add_data(212940) if($#regs<0);
			foreach my $reg(@regs){
				my $cmd="samtools view -q $opts{q} -c $bam $reg";
				my $nreads=`$cmd`;
				chomp $nreads;
				$readDensity{$bam}->add_data($nreads/$opts{G});

				if($#libs>0 || defined $opts{g}){
					my %nreadlib;
					my %nreadlibwin;
					my %nreadlibwin_lowqual;
					my $cmd="samtools view -q $opts{q} $bam $reg";
					my ($reg_chr,$reg_start,$reg_end)=($reg=~/^(\S+)\:(\d+)\-(\d+)/);
					my $start=$reg_start;
					open(SAM,"$cmd |") || die "unable to open $bam\n";
					while(<SAM>){
						my $rlb=basename($bam);
						if($_=~/RG\:Z\:(\S+)/){
							$rlb=$cfgs{readgroup_library}{$1};
						}
						next if(!defined $rlb);
						my @a = split(/\t/, $_);
						$nreadlib{$rlb}++;
						if($opts{g}){
							my ($rdname,$flag,$chr,$pos,$quality)=split;
							if($pos>=$start && $pos<$start+$opts{U}){
								$nreadlibwin{$rlb}++;
								if($quality < $opts{J}){
									$nreadlibwin_lowqual{$rlb} ++;
								}
							}
							elsif($pos>=$start+$opts{U}){
								foreach my $rrlib(keys %nreadlibwin){
# Ignore bins with low quality reads or very few reads for initialization
									next if($nreadlibwin{$rrlib} == 0 || defined $nreadlibwin_lowqual{$rrlib} && $nreadlibwin_lowqual{$rrlib}/$nreadlibwin{$rrlib} > $opts{Z});

									my $gc_frac = 50;
									if(defined $opts{g} && $opts{g} == 1){
										$gc_frac = int(&GCContent($reg_chr.':'.$start.'-'.($start+$opts{U}))*$opts{b});
									}
									if(!defined $gc_hist{$rrlib}{$gc_frac}){
										$gc_hist{$rrlib}{$gc_frac}=Statistics::Descriptive::Full->new();
									}
									$gc_hist{$rrlib}{$gc_frac}->add_data(int($nreadlibwin{$rrlib}/2+0.5));
									delete $nreadlibwin{$rrlib};
								}
								$nreadlibwin{$rlb}=1;
								$nreadlibwin_lowqual{$rlb} = 0;
								$start+=$opts{U};
							}
							else{}
						}
					}
					close(SAM);

					foreach my $lib(keys %nreadlib){
						if(!defined $stats{$lib}){
							$stats{$lib}=Statistics::Descriptive::Full->new();
						}
						$stats{$lib}->add_data(int($nreadlib{$lib}/2+0.5));
					}
				}
			}

			foreach my $lib(@libs){
# Estimate insert size density
				next if(!defined $stats{$lib});
				$cfgs{insert_density}{$lib}=int($stats{$lib}->median());
				$bin_size->{$lib} = &max($opts{I}*$opts{G}/$cfgs{insert_density}{$lib}, $opts{b});

				if($cfgs{insert_density}{$lib}<=0){
					print "$stats{$lib}->median()\n";
					die "$lib: Fail to estimate number of insert / position. Check if this is any wrong with the bam, e.g., did you samtools index bam?";
				}
				printf STDERR "#On average %d inserts mapped in %d bp window in library:%s.\n", $cfgs{insert_density}{$lib}, $opts{G},$lib;

				if(defined $opts{g}){
					open(GC,">gc.tmp.$lib");
					print GC "#GC\tMedian\n";
					foreach my $gc(keys %{$gc_hist{$lib}}){
						if($gc_hist{$lib}{$gc}->count()>=10){
							$cfgs{gc_hist}{$lib}{$gc/$opts{b}*$bin_size->{$lib}}=$gc_hist{$lib}{$gc}->median();
						}
					}
					$cfgs{gc_hist}{$lib}{1}=0;
					$cfgs{gc_hist}{$lib}{$bin_size->{$lib}}=0;

					my @gcs=sort {$a<=>$b} keys %{$cfgs{gc_hist}{$lib}};

# Linear Interprolation
					for(my $i=1;$i<=$#gcs;$i++){
						my ($start,$end)=($gcs[$i-1],$gcs[$i]);
						for(my $j=$start+1;$j<=$end-1;$j++){
							my $int=$cfgs{gc_hist}{$lib}{$start}+($cfgs{gc_hist}{$lib}{$end}-$cfgs{gc_hist}{$lib}{$start})*($j-$start)/($end-$start);
							$cfgs{gc_hist}{$lib}{$j}=$int;
						}
					}

					foreach my $gc(sort {$a<=>$b} keys %{$cfgs{gc_hist}{$lib}}){
						printf GC "%.2f\t%.2f\n",$gc,$cfgs{gc_hist}{$lib}{$gc};
					}
					close(GC);
				}
				print cfg_new "$bam\t$lib\tinsert_density:\t" . $cfgs{insert_density}{$lib} . "\n";
				print cfg_new "$bam\t$lib\tbin_size:\t";
				printf cfg_new "%.2f\n", $bin_size->{$lib};
			}
			$cfgs{median_readDensity}{$bam}=$readDensity{$bam}->median();
			print cfg_new "$bam\tNA\tmedian_readDensity:\t" . $cfgs{median_readDensity}{$bam} . "\n";
		} # end of if GC correction is on
# if no GC file and no GC correction
		else{
			foreach my $lib(@libs){
				if (defined $cfgs{insert_density}{$lib}) {
					$bin_size->{$lib} = &max($opts{I}*$opts{G}/$cfgs{insert_density}{$lib}, $opts{b});
				} else {
					$bin_size->{$lib} = $opts{b};
				} 
			}
		}
	}
	$cfgs{bams}=\@bams;
	close cfg_new if($f_gc eq "NA");

	if(!defined $opts{w}){$opts{w}=int($maxisize+0.5);}
	return \%cfgs;

}

sub read_gc_file{
	my ($gc_hist, $cfgs) = @_;
	foreach my $bam (keys %{$$cfgs{gc_file}}){
		foreach my $lib (keys %{$$cfgs{gc_file}{$bam}}){
			my $file_name = $$cfgs{gc_file}{$bam}{$lib};
			open fh_gc, "<$file_name" or die $!;
			while(<fh_gc>){
				next if($_ =~ /^#/);
				chomp;
				my ($gc, $median) = split("\t", $_);
				$gc = ceil($gc);
				$$cfgs{gc_hist}{$lib}{$gc} = $median;
			}
			close fh_gc;
		}
	}
}

sub read_new_cfg{
	my ($cfg_file_name, $cfgs, $config_file) = @_;
	open cfg_fh, "<$cfg_file_name" or die $!;
	while(<cfg_fh>){
		chomp;
		my @a = split(/\t/, $_);
		my $bam = $a[0];
		my $lib = $a[1];
		my $key = $a[2];
		my $number = $a[3];
		if(!-e $bam){
			my $dirname = dirname(abs_path($config_file));
			$bam = $dirname . "/" . $bam;
		}
		if($_ =~ /median_readDensity/){
			$$cfgs{median_readDensity}{$bam} = $number;
		}
		elsif($_ =~ /insert_density/){
			$$cfgs{insert_density}{$lib} = $number;
		}
		elsif($_ =~ /bin_size/){
			$bin_size->{$lib} = $number;
		}

	}
	close cfg_fh;
}

sub LAdd{
	my ($x, $y)=@_;
	my ($temp,$diff,$z);
	if ($x<$y) {
		$temp = $x; $x = $y; $y = $temp;
	}
	$diff = $y-$x;
	if ($diff<$minLogExp){
		return  ($x<$LSMALL)?$LZERO:$x;
	}
	else {
		$z = exp($diff);
		return $x+log(1.0+$z);
	}
	return $z;
}

# use 10 base the base
sub LAdd_base10{
	my ($x, $y)=@_;
	my ($temp,$diff,$z);
	if ($x<$y) {
		$temp = $x; $x = $y; $y = $temp;
	}
	$diff = $y-$x;
	if ($diff<$minLogExp){
		return  ($x<$LSMALL)?$LZERO:$x;
	}
	else {
		$z = (exp($diff))**$log10;
		return $x+log(1.0+$z)/$log10;
	}
	return $z;
}
sub LogPoissonPDF{
	my ($k,$lambda)=@_;
	my $logk_factorial=($k==0)?0:$k*log($k)-$k+0.5*log(2*$PI*$k);
	my $log_lambda=($lambda<=0)?$LSMALL:log($lambda);
	my $logp=$k*$log_lambda-$lambda-$logk_factorial;

	return $logp;
}

# use a different base from "e" to do Logarithm
sub LogPoissonPDF_base10{
	my ($k,$lambda)=@_;
	my $logk_factorial=($k==0)?0:$k*log($k)-$k+0.5*log(2*$PI*$k);
	my $log_lambda=($lambda<=0)?$LSMALL:log($lambda);
	my $logp=($k*$log_lambda-$lambda-$logk_factorial);

	return $logp;
}

sub max{
	my ($x, $y) = @_;
	return $y if($x < $y);
	return $x if($x >= $y);
}
